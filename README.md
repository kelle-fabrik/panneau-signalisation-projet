# Panneau Signalisation Projet

Ce projet a pour objectif la réalisation de panneau pour identifier les type de projet de l'association Kelle Fabrik.


<div align="center">
<img src="Images/20191016_141837.jpg" alt="drawing" width="450"/>
</div>



**Machine nécessaire :**

* Découpe Laser
* Plotteur vinyle
* Imprimante 3D

**Matériel utilisé :**

* Papier vinyle
* Filament PLA

**Logiciel utilisé :**

* FreeCAD
* Inskape


**Fichier à Disposition :**

* SVG : fichier source permetant la modification de l'information. Exploitable également pour la réalisation de découpe vinyl
* DXF : fichier exporter pour l'emplois à la découpe laser
* FCStd : Fichier source FreCAD, support panneau
* STL : fichier 3D version exporté depuis FreeCAD, support panneau

## Procédés

*1. Réalisation du fichier vectoriel sur inskape*

<img src="Images/adherent.png" alt="drawing" width="350"/>



*2. Découpe vinyle*

<img src="Images/Capture.PNG" alt="drawing" width="350"/>


*3. Gravure laser*

*4. Échenillage et pose du vinyle*

*5. Modélisation 3D du support*

*6. Impression 3D*


